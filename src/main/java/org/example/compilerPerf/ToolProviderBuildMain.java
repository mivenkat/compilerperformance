package org.example.compilerPerf;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

public class ToolProviderBuildMain {
    private static final int PROJECT_DIR_COUNT = 10;
    //    private static final int THREAD_COUNT = Runtime.getRuntime().availableProcessors();
    private static final int THREAD_COUNT = 1;

    public static void main(String[] args) throws IOException, InterruptedException {
        String parentPath = Constants.PARENT_PATH + "_" + UUID.randomUUID();
        String compileSubDir = "\\compile_" + System.nanoTime();

        Util.createSourceFiles(Constants.SINGLE_PKG_BOT_TOOL_PROV_SRC_PATH, parentPath + compileSubDir, PROJECT_DIR_COUNT);

        Thread.sleep(3000);

        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_COUNT);
        IntStream
                .range(1, PROJECT_DIR_COUNT + 1)
                .forEach(i ->
                    executorService.submit(() -> {
                            try {
                                final Path projectPath = Paths.get(parentPath + compileSubDir + "\\project" + i);
                                long start = System.nanoTime();
                                Util.compile(
                                        projectPath,
                                        Util.getCompileDeps(),
                                        Util.getClassPathEntries()
                                );
                                Util.createShadowJar(
                                        "bot" + i,
                                        projectPath,
                                        Util.getRuntimeDeps(),
                                        new HashSet<>()
                                );
                                System.out.println("Time to compile classes for [Thread: " +
                                        Thread.currentThread().getName() + "] is : " + (System.nanoTime() - start)/1_000_000_000.0);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    )
                );

        executorService.shutdown();
    }
}
