package org.example.compilerPerf;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

public class ToolProviderCompileMain {
    private static final int PROJECT_DIR_COUNT = 10;
    //    private static final int THREAD_COUNT = Runtime.getRuntime().availableProcessors();
    private static final int THREAD_COUNT = 4;


    public static void main(String[] args) throws InterruptedException, IOException {
        String parentPath = Constants.PARENT_PATH + "_" + UUID.randomUUID();
        String compileSubDir = "\\compile_" + System.nanoTime();

        Util.createSourceFiles(Constants.SINGLE_PKG_BOT_TOOL_PROV_SRC_PATH, parentPath + compileSubDir, PROJECT_DIR_COUNT);

        Thread.sleep(3000);

        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_COUNT);
        IntStream
            .range(1, PROJECT_DIR_COUNT + 1)
            .forEach(i ->
                executorService.submit(() -> {
                    try {
                        final Path projectPath = Paths.get(parentPath + compileSubDir + "\\project" + i);
                        System.out.println("Project path - " + projectPath);
                        Util.compile(
                            projectPath,
                            Util.getCompileDeps(),
                            Util.getClassPathEntries()
                        );
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            )
        );

        executorService.shutdown();
        //cleanUp(parentPath);
    }

}
