package org.example.compilerPerf;

import java.nio.file.Path;
import java.util.function.Function;

public interface Constants {
   String SINGLE_PKG_BOT_TOOL_PROV_SRC_PATH = "D:\\GeneratedCode\\SinglePkgBot\\ToolProvider";
   String SINGLE_PKG_BOT_GRADLE_PROV_SRC_PATH = "D:\\GeneratedCode\\SinglePkgBot\\GradleProvider";
   String MULTI_PKG_BOT_TOOL_PROV_SRC_PATH = "D:\\GeneratedCode\\MultiPkgBot\\ToolProvider";
   String MULTI_PKG_BOT_GRADLE_PROV_SRC_PATH = "D:\\GeneratedCode\\MultiPkgBot\\GradleProvider";
   String PARENT_PATH = "E:\\MultiThreadedCompilation";

   String BUILD_DIR = "build";
   String LIBS_DIR = "libs";
   String CLASSES_DIR = "classes";
   String JAR = "jar";
   String SOURCES = "sources";
   String BIN_DIR = "bin";
   String SOURCE = "src";
   String MAIN = "main";
   String MAIN_CLASS = "Main";
   String RESOURCES = "resources";
   String SHADOW_JAR_MANIFEST_VERSION = "1.0";
   String SHADOW_JAR_IMPLEMENTATION_TITLE = "Automation Anywhere Bot Jar";
   String SHADOW_JAR_IMPLEMENTATION_VERSION = "1.0-SNAPSHOT";
   String SHADOW_JAR_INTERACTIVE_KEY = "Interactive";
   String SHADOW_JAR_INTERACTIVE_VALUE = "False";
   String SHADOW_JAR_EXTERNAL_FUNCTIONS = "ExternalFunctions";
   String MAIN_CLASS_PACKAGE = "com.automationanywhere.botrunner";
   String GRADLE_PROPERTIES = "gradle.properties";
   String SHADOW_JAR = "shadowJar";
   String INCLUDE_PACKAGE_ARGS ="-PbundlePackages=true";
   String EXCLUDE_PACKAGE_ARGS ="-PbundlePackages=false";
   String GRADLE_HOME = "GRADLE_HOME";
   String GRADLE_BUILD = "build.gradle";
   String GRADLE_BUILD_RESOURCE = "main/resources/template/build.gradle";
   String PROJECT_GRADLE_CACHE_ARG = "--project-cache-dir";
   String BOT_GRADLE_CACHE_RELATIVE_PATH = "bot-gradle-cache";
   String UTF_8 = "UTF-8";
   String EXT_PROPERTIES_DIR = "ext.properties.dir";
   String PUBLIC_FOLDER = "public";
   String PACKAGE_FOLDER = "package";

   Function<Path, Path> libDirResolver = path -> path.resolve(Constants.BUILD_DIR).resolve(Constants.LIBS_DIR);
   Function<Path, Path> pubDirResolver = path -> path.resolve(PUBLIC_FOLDER).resolve(PACKAGE_FOLDER);
}
