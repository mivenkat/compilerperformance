package org.example.compilerPerf;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class JavacMain {
    private static final String CLASSES_DIR = "classes";
    private static final String SOURCES = "sources";
    private static final String BIN_DIR = "bin";
    //    private static final int THREAD_COUNT = Runtime.getRuntime().availableProcessors();
    private static final int THREAD_COUNT = 8;

    public static void main(String[] args) throws InterruptedException {
        cleanUp();

        Thread.sleep(3000);

        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_COUNT);
        IntStream.range(1, 101).forEach(i ->
            executorService.submit(() -> {
                try {
                    final Path projectPath = Paths.get("E:\\MultiThreadedCompilation\\project" + ((i%6) + 1));
                    System.out.println("Project path - " + projectPath);
                    compile(
                        projectPath,
                        "D:\\Zulu\\zulu11.33.15-ca-fx-jdk11.0.4-win_x64",
                        getCompileDeps(),
                        getClassPathEntries()
                    );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            })
        );

        //executorService.shutdown();
        //compilationExecutionPool.shutdown();

    }

    private static String sanitizePath(String path) {
        if (File.separatorChar == '\\') {
            return path.replace('\\', '/');
        } else {
            return path;
        }
    }

    private static void compile(final Path outPath, String javaBuildHome, List<String> compileDependencies, List<String> classPathEntries)
            throws IOException, ExecutionException, InterruptedException {
        System.out.println("Fetching the System Java compiler from Java Home - " + sanitizePath(javaBuildHome));
        String javac;
        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            javac = "javac.exe";
        }
        else {
            javac = "javac";
        }

        Path javaCompiler = Paths.get(sanitizePath(javaBuildHome), BIN_DIR, javac);
        if (!javaCompiler.toFile().exists()) {
            throw new RuntimeException("Java compiler path {} does not exist - " + javaCompiler.toFile().getAbsolutePath());
        }

//        System.out.println("Traversing the generated source files");
        List<String> srcFiles = Files.walk(outPath)
                .map(Path::toFile)
                .filter(f -> !f.isDirectory() && f.getName().endsWith(".java"))
                .map(f -> f.getAbsolutePath() + System.lineSeparator())
                .collect(Collectors.toList());
        Files.write(outPath.resolve("sources"), srcFiles, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

//        System.out.println("Creating the classes dir in output");
        forceMkdir(outPath.resolve(CLASSES_DIR).toFile());

        List<String> compileCommand = new ArrayList<>();
        compileCommand.add(sanitizePath(javaCompiler.toString()));
        compileCommand.add("-cp");

        StringBuilder strBuilder = new StringBuilder(
                compileDependencies.stream().reduce("",
                        (str1, str2) -> isEmpty(str1)? str2 + File.pathSeparator :  str1 + File.pathSeparator + str2)
        );
        strBuilder.append(File.pathSeparator);
        strBuilder.append(
                classPathEntries.stream().reduce("",
                        (str1, str2) -> isEmpty(str1)? str2 + File.pathSeparator :  str1 + File.pathSeparator + str2)
        );
        compileCommand.add(strBuilder.toString());
        compileCommand.add("-d");
        compileCommand.add(outPath.resolve(CLASSES_DIR).toString());
        compileCommand.add("@" + outPath.resolve(SOURCES).toString());

        System.out.println("Invoking Java compiler - " + compileCommand.toString());

        Process compileTask = null;
        int returnValue;
        StringBuilder sb = new StringBuilder();
        try {
            try {
                long start = System.nanoTime();
                ProcessBuilder builder = new ProcessBuilder(compileCommand);
                builder.redirectErrorStream(true);
                compileTask = builder.start();

                BufferedReader reader = new BufferedReader(
                    new InputStreamReader(compileTask.getInputStream(), StandardCharsets.UTF_8)
                );

                for (String line = ""; line != null; line = reader.readLine()) {
                    sb.append(line);
                }
                returnValue = compileTask.waitFor();
                System.out.println("Time to compile for current Thread " + Thread.currentThread().getName() + " is : " + (System.nanoTime() - start)/1_000_000_000.0);

                if (returnValue > 0) {
                    throw new RuntimeException("Bot compilation failed");
                } else {
                    System.out.println("Bot compilation succeeded");
                }
            } catch (Exception e) {
                throw new RuntimeException("Error creating java compilation process - " + e.getMessage());
            }

//            System.out.println("Compilation output: " + sb.toString());
        } finally {
            if (compileTask != null) {
                compileTask.destroyForcibly();
            }
        }
    }

    public static void forceMkdir(final File directory) throws IOException {
        if (directory.exists()) {
            if (!directory.isDirectory()) {
                final String message =
                        "File "
                                + directory
                                + " exists and is "
                                + "not a directory. Unable to create directory.";
                throw new IOException(message);
            }
        } else {
            if (!directory.mkdirs()) {
                // Double-check that some other thread or process hasn't made
                // the directory in the background
                if (!directory.isDirectory()) {
                    final String message =
                            "Unable to create directory " + directory;
                    throw new IOException(message);
                }
            }
        }
    }

    private static boolean isEmpty(final CharSequence cs) {
        return cs == null || cs.length() == 0;
    }

    private static List<String> getClassPathEntries() {
        return  new ArrayList<>(){
            {
                add("F:\\ProgramData\\AutomationAnywhere\\Server Files\\public\\package\\bot-command-messagebox-2.0.0-20191022-004306.jar");
            }
        };
    }

    private static List<String> getCompileDeps() {
        return new ArrayList<>() {
            {
                add("F:\\CR_Runner\\config\\lib\\aa-artifacts\\bot-api.jar");
                add("F:\\CR_Runner\\config\\lib\\aa-artifacts\\bot-runtime.jar");
                add("F:\\CR_Runner\\config\\lib\\aa-artifacts\\log4j-api-2.12.1.jar");
                add("F:\\CR_Runner\\config\\lib\\aa-artifacts\\protobuf-java-3.9.1.jar");
            }
        };
    }

    private static void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                if (! Files.isSymbolicLink(f.toPath())) {
                    deleteDir(f);
                }
            }
        }
        file.delete();
    }

    private static void cleanUp() {
        IntStream.range(1, 11).forEach(i -> {
            deleteDir(Paths.get("E:\\MultiThreadedCompilation\\project" + i + "\\classes").toFile());
        });
    }
}
