package org.example.compilerPerf;

import java.io.IOException;
import java.util.Properties;


public class CompilerPropertyReader {

    public static final String CONFIG_FILE_NAME = "bot.compiler.properties";
    private static final String BUILD_TEMPLATE_DIR = "build.main.resources.template.dir";
    public static final String FLAT_REPO_DIR = "flat.repo.dir";
    private static final String BUILD_JAVA_HOME = "build.java.home";
    public static final String FLAT_REPO_DIR2 = "flat.repo.dir2";
    public static final String GRADLE_INSTALL_DIR = "gradle.install.dir";
    private static final String BOT_API_VERSION = "bot.api.version";
    private static final String BOT_RUNTIME_VERSION = "bot.runtime.version";
    private static final String BOT_GRADLE_CACHE_RELATIVE_PATH = "bot.gradle.cache.relative.path";
    private static final String BOT_CACHE_RELATIVE_PATH = "bots/cache";
    private Properties properties;

    public CompilerPropertyReader() throws IOException {
        properties = Util.readExternalProperties(CONFIG_FILE_NAME);
    }

    public String getBuildTemplateDir(){
        return properties.getProperty(BUILD_TEMPLATE_DIR);
    }

    public String getFlatRepoDir(){
        return properties.getProperty(FLAT_REPO_DIR);
    }

    public String getFlatRepo2Dir(){
        return properties.getProperty(FLAT_REPO_DIR2);
    }

    public String getBotApiVersion() {
        return properties.getProperty(BOT_API_VERSION);
    }

    public String getBotRuntimeVersion() {
        return properties.getProperty(BOT_RUNTIME_VERSION);
    }

    public String getBotCacheRelativePath(){
        return properties.getProperty(BOT_CACHE_RELATIVE_PATH);
    }

    public String getBotGradleCacheRelativePath() {
        return properties.getProperty(BOT_GRADLE_CACHE_RELATIVE_PATH);
    }

    public String getGradleInstallDir(){
        return properties.getProperty(GRADLE_INSTALL_DIR);
    }

    public String getBuildJavaHome() { return properties.getProperty(BUILD_JAVA_HOME); }
}