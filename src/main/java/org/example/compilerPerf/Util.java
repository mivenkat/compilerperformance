package org.example.compilerPerf;

import javax.tools.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.jar.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.util.jar.Attributes.Name.MAIN_CLASS;
import static java.util.stream.Collectors.toList;


public class Util {

    private Util() {
        //nothing
    }

    static JavaCompiler getSystemJavaCompiler() {
        return ToolProvider.getSystemJavaCompiler();
    }

    static void compile(final Path outPath, List<String> compileDependencies, List<String> classPathEntries) {
        //System.out.println("Fetching the System Java compiler");
        JavaCompiler compiler = getSystemJavaCompiler();
        if (compiler == null) {
            throw new RuntimeException("Unable to fetch system Java compiler");
        }

        StandardJavaFileManager fileManager = null;
        try {
            //System.out.println("Traversing the generated source files");
            List<File> srcFiles = Files.walk(outPath)
                    .map(Path::toFile)
                    .filter(f -> !f.isDirectory() && f.getName().endsWith(".java"))
                    .collect(Collectors.toList());

            DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
            fileManager = compiler.getStandardFileManager(diagnostics,null, null);
            Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(srcFiles);
            classPathEntries.addAll(compileDependencies);
            fileManager.setLocation(
                    StandardLocation.CLASS_PATH,
                    classPathEntries.stream().map(File::new).collect(Collectors.toList())
            );

            //System.out.println("Creating the classes dir in output");
            forceMkdir(outPath.resolve(Constants.CLASSES_DIR).toFile());
            fileManager.setLocation(
                    StandardLocation.CLASS_OUTPUT,
                    Collections.singletonList(Paths.get(outPath.toString(), Constants.CLASSES_DIR).toFile())
            );

            //System.out.println("Invoking the java compiler");
//            long start = System.nanoTime();
            boolean compileStatus = compiler.getTask(
                    null,
                    fileManager,
                    diagnostics,
                    null,
                    null,
                    compilationUnits
            ).call();
//            System.out.println("Time to compile classes for [Thread: " +
//                    Thread.currentThread().getName() + "] is : " + (System.nanoTime() - start)/1_000_000_000.0);

            if (!compileStatus) {
                System.out.println("Bot compilation failed");
                StringBuilder sb = new StringBuilder();
                sb.append("\n");
                for (final Diagnostic<? extends JavaFileObject> diagnostic : diagnostics.getDiagnostics()) {
                    sb.append(
                            String.format(
                                    "%s in File: %s and line: %d  \n",
                                    diagnostic.getMessage(null),
                                    diagnostic.getSource().getName(),
                                    diagnostic.getLineNumber()
                            )
                    );
                }
                throw new RuntimeException("Bot compilation failed - " + sb.toString());
            }
            else {
//                System.out.println("Bot compilation succeeded");
            }
        } catch (IOException ex) {
            throw new RuntimeException("Bot compilation failed - ", ex);
        } finally {
            if (fileManager != null) {
                try {
                    fileManager.close();
                } catch (IOException e) {
                    System.out.println("Unable to close the standard java file manager");
                }
            }
        }
    }

    static void forceMkdir(final File directory) throws IOException {
        if (directory.exists()) {
            if (!directory.isDirectory()) {
                final String message = "File " + directory + " exists and is " + "not a directory. Unable to create directory.";
                throw new IOException(message);
            }
        } else {
            if (!directory.mkdirs()) {
                // Double-check that some other thread or process hasn't made
                // the directory in the background
                if (!directory.isDirectory()) {
                    final String message =
                            "Unable to create directory " + directory;
                    throw new IOException(message);
                }
            }
        }
    }

    static boolean isEmpty(final CharSequence cs) {
        return cs == null || cs.length() == 0;
    }

    static List<String> getCompileDeps() {
        return new ArrayList<>() {
            {
                add("F:\\CR_Runner\\config\\lib\\aa-artifacts\\bot-api.jar");
                add("F:\\CR_Runner\\config\\lib\\aa-artifacts\\bot-runtime.jar");
                add("F:\\CR_Runner\\config\\lib\\aa-artifacts\\log4j-api-2.12.1.jar");
                add("F:\\CR_Runner\\config\\lib\\aa-artifacts\\protobuf-java-3.9.1.jar");
            }
        };
    }

    static void cleanUp(String parentPath) {
        deleteDir(Paths.get(parentPath).toFile());
    }

    static void copyFolder(Path src, Path dest) throws IOException {
        Files
                .walk(src)
                .forEach(source -> {
                            try {
                                Files.copy(source, dest.resolve(src.relativize(source)), REPLACE_EXISTING);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                );
    }

    static void createSourceFiles(String sourceDir, String parentDir, int projectDirCount) throws IOException {
        Files.createDirectories(Paths.get(parentDir));
        IntStream.range(1, projectDirCount + 1).forEach(
                i -> {
                    try {
                        String projectDir = "project" + i;
                        Path projectPath = Paths.get(parentDir, projectDir);
                        copyFolder(Paths.get(sourceDir), projectPath);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        );
    }

    static void createShadowJar(
            String botName,
            Path outDir,
            List<String> runtimeDependencies,
            Set<UUID> externalFunctions
    ) {
//        System.out.println("Creating a shadow jar from botName : " + botName +
//                ", outDir: " + outDir + " and externalFunctions: " + externalFunctions.toString());

        JarOutputStream jarOutputStream = null;
        try {
//            long start = System.nanoTime();
            Files.createDirectories(Constants.libDirResolver.apply(outDir));
            Path jarFilePath = Files.createFile(Paths.get(Constants.libDirResolver.apply(outDir).toString(), botName + "." + Constants.JAR));
            Path sourceDirPath = outDir.resolve(Constants.CLASSES_DIR);
            jarOutputStream = new JarOutputStream(Files.newOutputStream(jarFilePath), createJarManifest(externalFunctions));
            Set<String> destJarFileEntries = new HashSet<>();
            copyCompiledClassesToShadowJar(sourceDirPath, destJarFileEntries, jarOutputStream);
            copyResourcesToShadowJar(outDir.resolve(Constants.SOURCE).resolve(Constants.MAIN).resolve(Constants.RESOURCES), destJarFileEntries, jarOutputStream);
            copyLibJarToShadowJar(destJarFileEntries, runtimeDependencies, jarOutputStream);
//            System.out.println("Time  to create to Shadow Jar - " + botName + ".jar  in "
//                    + "in [Thread: " + Thread.currentThread().getName()  + " ] is - " + (System.nanoTime() - start)/1_000_000_000.0);
        }
        catch (IOException e) {
            System.out.println("Failed to create shadow jar" +  e);
            throw new RuntimeException("Bot jar creation failed", e);
        }
        finally {
            if (jarOutputStream != null) {
                try {
                    jarOutputStream.close();
                } catch (IOException e) {
                    System.out.println("Unable to close jar stream");
                }
            }
        }
//        System.out.println("Shadow jar creation succeeded");
    }

    static Manifest createJarManifest(Set<UUID> externalFunctions) {
//        System.out.println("Creating jar manifest");
        Manifest jarManifest = new Manifest();
        List<String> functions = externalFunctions.stream().map(UUID::toString).collect(toList());
        jarManifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, Constants.SHADOW_JAR_MANIFEST_VERSION);
        jarManifest.getMainAttributes().put(Attributes.Name.IMPLEMENTATION_TITLE, Constants.SHADOW_JAR_IMPLEMENTATION_TITLE);
        jarManifest.getMainAttributes().put(Attributes.Name.IMPLEMENTATION_VERSION, Constants.SHADOW_JAR_IMPLEMENTATION_VERSION);
        jarManifest.getMainAttributes().put(new Attributes.Name(Constants.SHADOW_JAR_INTERACTIVE_KEY), Constants.SHADOW_JAR_INTERACTIVE_VALUE);
        jarManifest.getMainAttributes().put(MAIN_CLASS, Constants.MAIN_CLASS_PACKAGE + "." + MAIN_CLASS);
        jarManifest.getMainAttributes().put(new Attributes.Name(Constants.SHADOW_JAR_EXTERNAL_FUNCTIONS), String.join(",", functions));
//        System.out.println("Jar manifest creation succeeded");
        return jarManifest;
    }

    static void copyCompiledClassesToShadowJar(
            final Path sourceDirPath,
            Set<String> destJarFileEntries,
            JarOutputStream js
    ) {
        //Copy class files to Jar Stream
//        System.out.println("Copying compiled classes to Jar output stream");
        copyFilesToShadowJar(sourceDirPath, destJarFileEntries, js);
//        System.out.println("Copied compiled classes to Jar output stream");
    }

    static void copyResourcesToShadowJar(
            Path resourcesPath,
            Set<String> destJarFileEntries,
            JarOutputStream js
    ) {
        //Copy resource files to Jar Stream
        if (resourcesPath.toFile().exists() && resourcesPath.toFile().isDirectory()) {
            //System.out.println("Copying resources to Jar output stream");
            copyFilesToShadowJar(resourcesPath, destJarFileEntries, js);
//            System.out.println("Copied resources to Jar output stream");
        }
    }

    static void copyFilesToShadowJar(Path sourceDirPath, Set<String> destJarFileEntries, JarOutputStream js) {
        try (Stream<Path> pathStream = Files.walk(sourceDirPath)) {
            pathStream
                    .filter(path -> !Files.isDirectory(path))
                    .forEach(classFilePath -> {
                        String name = sourceDirPath.relativize(classFilePath).toString().replace("\\", "/");
                        ZipEntry zipEntry = new ZipEntry(name);
                        try {
                            js.putNextEntry(zipEntry);
                            destJarFileEntries.add(zipEntry.getName());
                            Files.copy(classFilePath, js);
                            js.closeEntry();
                        } catch (IOException ex) {
                            throw new RuntimeException("Unable to copy " + zipEntry.getName() + " to JAR stream : " + ex);
                        }
                    });
        } catch (IOException ex) {
            throw new RuntimeException("Unable to traverse source java files - " + ex);
        }
    }

    static void copyLibJarToShadowJar(Set<String> destJarFileEntries, List<String> runtimeDependencies, JarOutputStream js) {
        //Copy jar class files to destination jar stream
        runtimeDependencies.forEach(libraryJarPath -> {
            try {
                ZipFile libJarFile = new ZipFile(libraryJarPath);
                Enumeration<?> libEntryEnumerator = libJarFile.entries();
                while (libEntryEnumerator.hasMoreElements()) {
                    ZipEntry srcJarEntry = (ZipEntry) libEntryEnumerator.nextElement();
                    if (destJarFileEntries.contains(srcJarEntry.getName()) || srcJarEntry.getName().toUpperCase().startsWith("META-INF")) {
                        continue;
                    }
                    try (InputStream inputStream = libJarFile.getInputStream(srcJarEntry)) {
                        ZipEntry zipEntry = new ZipEntry(srcJarEntry.getName());
                        js.putNextEntry(zipEntry);
                        destJarFileEntries.add(zipEntry.getName());
                        byte[] buffer = new byte[100 * 1024];
                        int len;
                        while ((len = inputStream.read(buffer)) != -1) {
                            js.write(buffer, 0, len);
                        }
                        js.closeEntry();
                    }
                }
            } catch (IOException ex) {
                throw new RuntimeException("JAR entry creation failed", ex);
            }
        });
    }

    static void copyLibJarToShadowJar(Set<String> destJarFileEntries, Map<JarEntry, byte[]> templateJarContent, JarOutputStream js) {
        //Copy jar class files to destination jar stream
        templateJarContent
                .entrySet()
                .stream()
                .filter(jarEntry -> !destJarFileEntries.contains(jarEntry.getKey().getName()))
                .forEach(jarEntry -> {
                    try {
                        JarEntry zipEntry = new JarEntry(jarEntry.getKey().getName());
                        js.putNextEntry(zipEntry);
                        destJarFileEntries.add(zipEntry.getName());
                        js.write(jarEntry.getValue(), 0, jarEntry.getValue().length);
                        js.closeEntry();
                    } catch (IOException e) {
                        throw new RuntimeException("JAR entry creation failed", e);
                    }
                });
    }

    static List<String> getClassPathEntries() {
        return  new ArrayList<>(){
            {
                add("F:\\ProgramData\\AutomationAnywhere\\Server Files\\public\\package\\bot-command-messagebox-2.0.0-20191030-131647.jar");
            }
        };
    }

    static List<String> getRuntimeDeps() {
        return new ArrayList<>() {
            {
                add("F:\\CR_Runner\\config\\lib\\aa-artifacts\\bot-api.jar");
                add("F:\\CR_Runner\\config\\lib\\aa-artifacts\\bot-runtime.jar");
            }
        };
    }


    static void cleanUp() {
        IntStream.range(1, 11).forEach(i -> {
            deleteDir(Paths.get("E:\\MultiThreadedCompilation\\project" + i + "\\build").toFile());
        });
    }

    static void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                if (! Files.isSymbolicLink(f.toPath())) {
                    deleteDir(f);
                }
            }
        }
        file.delete();
    }

    static boolean isNotBlank(final CharSequence cs) {
        return !isBlank(cs);
    }

    static boolean isBlank(final CharSequence cs) {
        int strLen;
        if (cs == null || (strLen = cs.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(cs.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    static String toStr(final byte[] bytes, final String charsetName) throws UnsupportedEncodingException {
        return charsetName != null ? new String(bytes, charsetName) : new String(bytes, Charset.defaultCharset());
    }

    static Properties readExternalProperties(String file) throws IOException {
        String propDir = System.getProperty(Constants.EXT_PROPERTIES_DIR, "");
        Properties properties = new Properties();

        // load default properties first
        try (InputStream in = Util.class.getClassLoader().getResourceAsStream(file)) {
            if (in != null) {
                properties.load(in);
            }
        }

//        System.out.println("Default properties loaded + " + file);

        if (!propDir.isEmpty()) {
            System.out.println("External properties directory set to " + propDir);
            // check whether there is an override file or not
            Path propFilePath = Paths.get(propDir).resolve(file);

            System.out.println("Loading properties from " + propFilePath.toString());
            try {
                try (InputStream in = new FileInputStream(propFilePath.toFile())) {
                    properties.load(in);
                    System.out.println("Loaded properties from " + propFilePath.toFile());
                }
            } catch (IOException e) {
                System.out.println("Could not load external properties: " + e.getMessage());
            }
        }
        return properties;
    }

    static void copyDir(Path sourceDir, Path targetDir) throws IOException {
        Files.walk(sourceDir)
            .forEach(s ->
            {
                try {
                    Path d = targetDir.resolve(sourceDir.relativize(s));
                    if (Files.isDirectory(s)) {
                        Files.createDirectories(d);
                        return;
                    }
                    Files.copy(s, d);// use flag to override existing
                } catch (Exception e) {
                    throw new RuntimeException("Unable to copy directory", e);
                }
            });
    }

    static void createShadowJar(
      String botName,
      Path outDir,
      Map<JarEntry, byte[]> templateJarContent,
      Set<UUID> externalFunctions
    ) {
        JarOutputStream jarOutputStream = null;
        try {
            Files.createDirectories(Constants.libDirResolver.apply(outDir));
            Path jarFilePath = Files.createFile(Paths.get(Constants.libDirResolver.apply(outDir).toString(), botName + "." + Constants.JAR));
            Path sourceDirPath = outDir.resolve(Constants.CLASSES_DIR);
            jarOutputStream = new JarOutputStream(Files.newOutputStream(jarFilePath), createJarManifest(externalFunctions));
            Set<String> destJarFileEntries = new HashSet<>();
            copyCompiledClassesToShadowJar(sourceDirPath, destJarFileEntries, jarOutputStream);
            copyResourcesToShadowJar(outDir.resolve(Constants.SOURCE).resolve(Constants.MAIN).resolve(Constants.RESOURCES), destJarFileEntries, jarOutputStream);
            copyLibJarToShadowJar(destJarFileEntries, templateJarContent, jarOutputStream);
        }
        catch (IOException e) {
            throw new RuntimeException("Copy classes to template jar failed", e);
        }
        finally {
            if (jarOutputStream != null) {
                try {
                    jarOutputStream.close();
                } catch (IOException e) {
                    System.out.println("Unable to close shadow jar stream");
                }
            }
        }
    }

    static Map<JarEntry, byte[]> getTemplateJarContent(List<String> runtimeDependencies) {
        JarOutputStream jarOutputStream = null;
        try {
            Map<JarEntry, byte[]> templateJarEntries = new ConcurrentHashMap<>();
            runtimeDependencies.forEach(libraryJarPath -> {
                try {
                    JarFile libJarFile = new JarFile(libraryJarPath);
                    Enumeration<?> libEntryEnumerator = libJarFile.entries();
                    while (libEntryEnumerator.hasMoreElements()) {
                        JarEntry srcJarEntry = (JarEntry) libEntryEnumerator.nextElement();
                        if (srcJarEntry.getName().toUpperCase().startsWith("META-INF")) {
                            continue;
                        }
                        try (InputStream inputStream = libJarFile.getInputStream(srcJarEntry)) {
                            ByteArrayOutputStream byteOutStream = null;
                            try {
                                byte[] buffer = new byte[100 * 4096];
                                byteOutStream = new ByteArrayOutputStream();
                                int read;
                                while ((read = inputStream.read(buffer)) != -1) {
                                    byteOutStream.write(buffer, 0, read);
                                }
                                templateJarEntries.put(srcJarEntry, byteOutStream.toByteArray());
                            }   finally {
                                try {
                                    if (byteOutStream != null) {
                                        byteOutStream.close();
                                    }
                                } catch (IOException e) {
                                }
                            }
                        }
                    }
                } catch (IOException ex) {
                    throw new RuntimeException("JAR entry creation failed", ex);
                }
            });
            return templateJarEntries;
        }
        finally {
            if (jarOutputStream != null) {
                try {
                    jarOutputStream.close();
                } catch (IOException e) {
                    System.out.println("Unable to close template jar stream");
                }
            }
        }
    }
}
