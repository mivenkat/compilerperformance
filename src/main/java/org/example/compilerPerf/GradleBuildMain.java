package org.example.compilerPerf;

import org.apache.commons.io.FileUtils;
import org.gradle.tooling.GradleConnectionException;
import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ProjectConnection;
import org.gradle.tooling.internal.consumer.DefaultBuildLauncher;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class GradleBuildMain {
    private static final int PROJECT_DIR_COUNT = 10;
    //    private static final int THREAD_COUNT = Runtime.getRuntime().availableProcessors();
    private static final int THREAD_COUNT = 8;


    public static void main(String[] args) throws IOException, InterruptedException {
        String parentPath = Constants.PARENT_PATH + "_" + UUID.randomUUID();
        String compileSubDir = "\\compile_" + System.nanoTime();
        Util.createSourceFiles(Constants.SINGLE_PKG_BOT_GRADLE_PROV_SRC_PATH, parentPath + compileSubDir, PROJECT_DIR_COUNT);

        Thread.sleep(3000);

        Path packageDir = Paths.get("F:\\ProgramData\\AutomationAnywhere\\Server Files");
        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_COUNT);
        IntStream
            .range(1, PROJECT_DIR_COUNT + 1)
            .forEach(i ->
                executorService.submit(() -> {
                        try {
                            final CompilerPropertyReader propertyReader = new CompilerPropertyReader();
                            final Path outPath = Paths.get(parentPath + compileSubDir + "\\project" + i);
                            String botName = "bot" + i;
                            long start = System.nanoTime();
                            copyBuildTemplate(outPath, propertyReader);
                            createGradlePropertyFile(outPath, packageDir, botName, new HashSet<>(), propertyReader);
                            buildInternal(outPath, propertyReader);
                            System.out.println("Time to compile bot - " + botName +
                                    " in [Thread " + Thread.currentThread().getName() + "] is - "
                                    + (System.nanoTime() - start)/1_000_000_000.0);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                )
            );
        executorService.shutdown();
    }

    static void buildInternal(Path outDir, CompilerPropertyReader propertyReader) {
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        GradleConnector gradleConnector = null;
        try {
            gradleConnector = GradleConnector.newConnector();
        } catch (Exception e) {
            e.printStackTrace();
        }
        gradleConnector.forProjectDirectory(outDir.toFile());

        File gradleInstallDir = findGradleInstallDir(propertyReader);
        if (null != gradleInstallDir) {
            gradleConnector.useInstallation(gradleInstallDir);
        }
        ProjectConnection connection = null;

        try {
            connection = gradleConnector.connect();
            DefaultBuildLauncher launcher = (DefaultBuildLauncher) connection.newBuild();
            launcher.withArguments(Constants.PROJECT_GRADLE_CACHE_ARG +  "=" + getBotGradleCachePath(propertyReader).toString());
            launcher.forTasks(Constants.SHADOW_JAR);
            launcher.setStandardOutput(outputStream);
            launcher.setStandardError(outputStream);
            launcher.run();
        } catch (GradleConnectionException t) {
            try {
                String compilationErrors = Util.toStr(outputStream.toByteArray(), Constants.UTF_8);
                throw new RuntimeException("Compilation failed " + compilationErrors, t);
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("UnsupportedEncodingException " + t);
            }

        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    static void createGradlePropertyFile(Path outDir,
                                         Path packageDir,
                                         String botName,
                                         Set<UUID> externalFunctions,
                                         CompilerPropertyReader propertyReader) {
        try {
            Path path = Files.createFile(outDir.resolve(Constants.GRADLE_PROPERTIES));
            PrintWriter writer = new PrintWriter(path.toFile(), "UTF-8");
            writer.println(String.format("jar_name=%s", botName));
            writer.println("jar_version=1.0-SNAPSHOT");
            writer.println(String.format("main_class=%s.%s", Constants.MAIN_CLASS_PACKAGE, Constants.MAIN_CLASS));

            List<String> functions = externalFunctions.stream()
                    .map(UUID::toString)
                    .collect(toList());

            writer.println(String.format("ext_func=%s", String.join(",", functions)));

            //Populate the package repo dir derived from repository path
            String packageRepoPath = Constants.pubDirResolver.apply(packageDir).toString();
            if (File.separatorChar == '\\') {
                packageRepoPath = packageRepoPath.replace('\\', '/');
                if (packageRepoPath.startsWith("//")) {
                    packageRepoPath = packageRepoPath.replaceAll(" ", "%20");
                }
            }
            writer.println(String.format("package_repo_dir=%s", packageRepoPath));

            if (Util.isNotBlank(propertyReader.getFlatRepoDir())) {
                System.out.println("Found flat.repo.dir '{}' " + propertyReader.getFlatRepoDir() + ", adding to gradle properties" );
                writer.println(String.format("flat_repo_dir=%s", propertyReader.getFlatRepoDir()));
            }

            if (Util.isNotBlank(propertyReader.getBuildJavaHome())) {
                writer.println(String.format("org.gradle.java.home=%s", propertyReader.getBuildJavaHome()));
            }

            writer.println(String.format("flat_repo_dir2=%s",propertyReader.getFlatRepo2Dir()));
            writer.println(String.format("bot_api_version=%s", propertyReader.getBotApiVersion()));
            writer.println(String.format("bot_runtime_version=%s", propertyReader.getBotRuntimeVersion()));
            writer.println("org.gradle.caching=true");
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException("Error creating gradle properties file", e);
        }
    }

    static void copyBuildTemplate(Path outDir, CompilerPropertyReader propertyReader) {
        try {
//            InputStream inputStream = GradleBuildMain.class.getClass().getResourceAsStream(Constants.GRADLE_BUILD_RESOURCE);
            InputStream inputStream = new FileInputStream("E:\\code_new\\CompilerPerf\\src\\main\\resources\\template\\build.gradle");
            FileUtils.copyInputStreamToFile(inputStream, outDir.resolve(Constants.GRADLE_BUILD).toFile());
        } catch (IOException e) {
            System.out.println("Unable to copy build.gradle from resources, trying from build.main.resources.template directory..." + e);

            try {
                Path sourceDir = Paths.get(propertyReader.getBuildTemplateDir());
                if (sourceDir.toFile().exists()) {
                    Util.copyDir(sourceDir, outDir);
                    System.out.println("build.main.resources.template directory - " +  sourceDir + " copied to outDir:  " + outDir);
                } else {
                    System.out.println("Unable to find build.main.resources.template directory, cannot copy build.gradle file");
                }
            } catch (IOException e1) {
                throw new RuntimeException("Unable to copy build main.resources.template file - ", e1);
            }
        }
    }

    static File findGradleInstallDir(CompilerPropertyReader compilerPropertyReader) {
        String gradleDirFromProperties = compilerPropertyReader.getGradleInstallDir();
        if (null != gradleDirFromProperties) {
            return Paths.get(gradleDirFromProperties).toFile();
        }
        String gradleHome = System.getenv(Constants.GRADLE_HOME);
        if (null != gradleHome) {
            return Paths.get(gradleHome).toFile();
        }

        return null;
    }

    static Path getBotGradleCachePath(CompilerPropertyReader compilerPropertyReader) {
        return Paths.get(System.getProperty("java.io.tmpdir"))
                .resolve(getGradleCacheRelativePath(compilerPropertyReader));
    }

    static String getGradleCacheRelativePath(CompilerPropertyReader compilerPropertyReader) {
        String relativeDirPath = "";
        relativeDirPath = compilerPropertyReader.getBotGradleCacheRelativePath();
        if (Util.isBlank(relativeDirPath)) {
            relativeDirPath = Constants.BOT_GRADLE_CACHE_RELATIVE_PATH;
        }
        return relativeDirPath;
    }
}
