package org.example.compilerPerf;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

public class ZipFileMain {
    //    private static final int THREAD_COUNT = Runtime.getRuntime().availableProcessors();
    private static final int THREAD_COUNT = 2;

    public static void main(String[] args) throws IOException, InterruptedException {
        Util.cleanUp();

        Thread.sleep(3000);

        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_COUNT);
        IntStream.range(1, 11).forEach(i ->
            executorService.submit(() -> {
                try {
                    long start = System.nanoTime();
                    Util.createShadowJar(
                            "bot" + i,
                            Paths.get("E:\\MultiThreadedCompilation\\project" + i),
                            Util.getRuntimeDeps(),
                            new HashSet<>()
                    );
                    System.out.println("Time to create shadow jar " + (System.nanoTime() - start)/1_000_000_000.0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            })
        );

        executorService.shutdown();
    }

}
